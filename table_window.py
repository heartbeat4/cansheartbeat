import matplotlib.pyplot as plt
class TableWindow:
    def __init__(self, data):
        self.data = data

    def show(self):
        # Process data to generate table
        # For demonstration purposes, let's assume 'data' is a list of tuples: (date, hour, value, pulse)
        dates = [entry[0] for entry in self.data]
        hours = [entry[1] for entry in self.data]
        values = [entry[2] for entry in self.data]
        pulses = [entry[3] for entry in self.data]

        # Plot the table
        plt.figure(figsize=(10, 6))
        for i, (date, hour, value, pulse) in enumerate(self.data):
            plt.text(i, 0.5, f'{value}\n{pulse}', ha='center', va='center')
        plt.xticks(range(len(self.data)), dates, rotation=45)
        plt.yticks([])
        plt.title('Blood Pressure Table')
        plt.xlabel('Date')
        plt.ylabel('Hour')
        plt.tight_layout()
        plt.show()
