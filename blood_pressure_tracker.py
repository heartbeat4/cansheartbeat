from imports import *

class MyWidget(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Tansiyon')
        app = QApplication.instance()
        app.setApplicationName("Tansiyon")
        
        # Replace 'path_to_icon.ico' with the actual path to your icon file
        icon_path = os.path.abspath("health.png")
        self.setWindowIcon(QIcon(icon_path))  
        
        menubar = self.menuBar()
        menubar.setNativeMenuBar(False)
        
        about_menu = menubar.addMenu('Menü')
                
        #Add action to the "about" menu
        about_action = QAction('&About Tansiyon',self)
        about_action.triggered.connect(self.show_about)
        about_menu.addAction(about_action)
        
        # Add action to the show table
        show_table_action = QAction('&Show Table', self)
        show_table_action.triggered.connect(self.show_table)
        about_menu.addAction(show_table_action)
        
        
        self.HighLabel = QLabel("Enter High value:")
        self.HighValue = QLineEdit()
        self.LowLabel = QLabel("Enter Low value:")
        self.LowValue = QLineEdit()
        self.PulseLabel = QLabel("Enter Pulse:")
        self.PulseValue = QLineEdit()
        self.TimeLabel = QLabel("Enter Time:")
        self.TimeValue = QLineEdit()
        self.DayLabel = QLabel("Enter Date:")
        self.DateValue = QLineEdit()
        self.add_button = QPushButton("Add")
        self.add_button.clicked.connect(self.enter_data)
        self.quit_button = QPushButton("Quit")
        self.quit_button.clicked.connect(self.close)
        
        layout = QVBoxLayout()
        layout.addWidget(self.HighLabel)
        layout.addWidget(self.HighValue)
        layout.addWidget(self.LowLabel)
        layout.addWidget(self.LowValue)
        layout.addWidget(self.PulseLabel)
        layout.addWidget(self.PulseValue)
        layout.addWidget(self.TimeLabel)
        layout.addWidget(self.TimeValue)
        layout.addWidget(self.DayLabel)
        layout.addWidget(self.DateValue)
        layout.addWidget(self.add_button)
        layout.addWidget(self.quit_button)
        
        #self.setLayout(layout)
        central_widget = QWidget()
        central_widget.setLayout(layout)
        self.setCentralWidget(central_widget)
        
    def enter_data(self):
        high_value=self.HighValue.text()
        low_value=self.LowValue.text()
        date_value=self.DateValue.text()
        time_value=self.TimeValue.text()
        pulse_value=self.PulseValue.text()
        
        if not high_value or not low_value or not date_value or not time_value or not pulse_value:
            QMessageBox.warning(self, "Warning", "Enter all data")
            return
        if not re.match(r'^\d{2}-\d{2}-\d{2}$', date_value):
            QMessageBox.warning(self, "Warning", "Enter date in format dd-mm-yy")
            return
        if not re.match(r'^\d{2}:\d{2}$', time_value):
            QMessageBox.warning(self, "Warning", "Enter time in format hh:mm")
            return
        
        with open('time_log.txt', 'a') as file:
            file.write(f"{date_value} - {time_value} - {high_value} - {low_value} - {pulse_value}\n")

    def show_about(self):
        QMessageBox.about(self, "About Tansiyon", "Can")
        
    def show_table(self):
        # Read data from file
        with open('time_log.txt', 'r') as file:
            data = [line.strip().split(' - ') for line in file.readlines()]
        # Debugging print statement
        print("Data from file:", data)
        # Open new window and display table
        table_window = TableWindow(data)
        table_window.show()
    

def main():
    app = QApplication(sys.argv)
    app.setApplicationName("Tansiyon")  # Set the application name
    app.setWindowIcon(QIcon('health.png'))
    widget = MyWidget()
    widget.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
    
        